#!/bin/bash

# Install Docker
sudo apt-get install     apt-transport-https     ca-certificates     curl     gnupg2     software-properties-common
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -
sudo apt-key fingerprint 0EBFCD88
sudo add-apt-repository    "deb [arch=amd64] https://download.docker.com/linux/debian \
   $(lsb_release -cs) \
   stable"
apt search add-apt-repository 
apt search add-apt-repository
apt-get install software-properties-common
sudo add-apt-repository    "deb [arch=amd64] https://download.docker.com/linux/debian \
   $(lsb_release -cs) \
   stable"
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-compose

# Icecast
mkdir -p /home/dockers
git clone git@gitlab.com:noslivres/radio-online.git
cd radio-online
docker-compose up -d

 
